import { TableInstance } from "react-table";
import styled from "styled-components";

import { TableBody } from "./table-body";
import { TableHead } from "./table-head";

const StyledTable = styled.div`
  display: inline-block;
  border-spacing: 0;
  border: 1px solid black;
`;

export interface ITableProps {
  instance: TableInstance<any>;
}

export const Table = ({ instance }: ITableProps) => {
  const { getTableProps, headerGroups } = instance;

  return (
    <StyledTable {...getTableProps()}>
      <TableHead headerGroups={headerGroups} />
      <TableBody instance={instance} />
    </StyledTable>
  );
};
