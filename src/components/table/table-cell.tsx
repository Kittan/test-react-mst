import { observer } from "mobx-react-lite";
import { useEffect, useRef, useState } from "react";
import { Cell } from "react-table";
import { toast } from "react-toastify";
import styled from "styled-components";

import { useMst } from "../../store";

const StyledCell = styled.div`
  margin: 0;
  padding: 0.5rem;
  border-bottom: 1px solid black;
  border-right: 1px solid black;
  display: flex !important;
  align-items: center;
  justify-content: center;
  transition: background-color ease 0.2s;
  cursor: pointer;

  :last-child {
    border-right: 0;
  }

  :hover {
    background-color: rgba(0, 0, 0, 0.1);
  }
`;

interface IProps {
  cell: Cell<any>;
}

export const TableCell = observer(({ cell }: IProps) => {
  const { postModel } = useMst();
  const [countPost, setCountPost] = useState<null | number>(null);
  const isFocusEmail = useRef(false);

  const isEmail = cell.column.id === "email";

  useEffect(() => {
    if (isEmail) {
      const newCountPost = postModel.getCountPostsFromUser(
        String(cell.row.values.id)
      );

      if (typeof newCountPost === "undefined") return;

      setCountPost(newCountPost);
    }
  }, [cell.row.values.id, isEmail, postModel, postModel.posts.size]);

  useEffect(() => {
    if (isEmail && isFocusEmail.current && typeof countPost === 'number') {
      toast.dismiss();
      toast.info(`Количество постов: ${countPost}`, { theme: "dark" });
    }
  }, [countPost, isEmail]);

  const onMouseOver = () => {
    if (isEmail) {
      isFocusEmail.current = true;
      if (countPost !== null) {
        toast.dismiss();
        toast.info(`Количество постов: ${countPost}`, { theme: "dark" });
      } else {
        toast.dismiss();
        toast.loading("Загрузка постов");
      }
    }
  };

  const onMouseOut = () => {
    isFocusEmail.current = false;
    toast.dismiss();
  };

  return (
    <StyledCell
      {...cell.getCellProps()}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
    >
      {cell.render("Cell")}
    </StyledCell>
  );
});
