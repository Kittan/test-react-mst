import { HeaderGroup } from "react-table";
import styled from "styled-components";

const StyleHead = styled.div``;

const StyledRow = styled.div`
  :last-child {
    .td {
      border-bottom: 0;
    }
  }
`;

const StyledCell = styled.div`
  margin: 0;
  padding: 0.5rem;
  border-bottom: 1px solid black;
  border-right: 1px solid black;
  display: flex !important;
  align-items: center;
  justify-content: center;

  :last-child {
    border-right: 0;
  }
`;

interface IProps {
  headerGroups: HeaderGroup<any>[];
}

export const TableHead = ({ headerGroups }: IProps) => (
  <StyleHead>
    {headerGroups.map((headerGroup) => (
      <StyledRow {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map((column) => (
          <StyledCell {...column.getHeaderProps()}>
            {column.render("Header")}
          </StyledCell>
        ))}
      </StyledRow>
    ))}
  </StyleHead>
);
