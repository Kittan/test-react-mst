import { Row } from "react-table";
import styled from "styled-components";

import { TableCell } from "./table-cell";

const StyledRow = styled.div`
  :last-child {
    border-bottom: 0;
  }
`;

interface IProps {
  row: Row<any>;
  style: React.CSSProperties;
}

export const TableRow = ({ row, style }: IProps) => (
  <StyledRow {...row.getRowProps({ style })}>
    {row.cells.map((cell) => (
      <TableCell key={cell.value} cell={cell} />
    ))}
  </StyledRow>
);
