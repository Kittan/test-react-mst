import { useCallback } from "react";
import { FixedSizeList } from "react-window";
import styled from "styled-components";

import { ITableProps } from ".";
import { TableRow } from "./table-row";

const StyledBody = styled.div``;

export const TableBody = ({ instance }: ITableProps) => {
  const { getTableBodyProps, rows, prepareRow, totalColumnsWidth } = instance;

  const renderRow = useCallback(
    ({ index, style }) => {
      const row = rows[index];
      prepareRow(row);

      return <TableRow key={row.id} row={row} style={style} />;
    },
    [prepareRow, rows]
  );

  return (
    <StyledBody {...getTableBodyProps()}>
      <FixedSizeList
        className="virtual-list"
        height={600}
        itemCount={rows.length}
        itemSize={60}
        width={totalColumnsWidth}
      >
        {renderRow}
      </FixedSizeList>
    </StyledBody>
  );
};
