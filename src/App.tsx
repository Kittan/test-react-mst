import { Router } from "./router";
import { StoreProvider, rootStore } from "./store";

const App = () => (
  <StoreProvider value={rootStore}>
    <Router />
  </StoreProvider>
);

export default App;
