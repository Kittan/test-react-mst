import axios from "axios"
import { API_URL } from "./config"

export const getUsersFromApi = (page: number) => {
	return axios.get(`${API_URL}/users?page=${page}`)
}

export const getUserPostsFromApi = (id: string) => {
	return axios.get(`${API_URL}/users/${id}/posts`)
}