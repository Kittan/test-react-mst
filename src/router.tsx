import { HashRouter, Route, Routes } from "react-router-dom";
import { RoutesEnum } from "./enums/routes.enum";
import { Users } from "./pages/users";

export const Router = () => (
  <HashRouter>
    <Routes>
      <Route path={RoutesEnum.Home} element={<Users />} />
    </Routes>
  </HashRouter>
)
