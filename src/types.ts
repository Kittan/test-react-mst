export interface IUser {
	id: number
	name: string
	email: string
	gender: 'male' | 'female',
	status: 'active' | 'inactive'
}

export interface IPost {
	id: number
	user_id: number
	title: string
	body: string
}