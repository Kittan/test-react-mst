import { flow, types } from "mobx-state-tree";

import { getUsersFromApi } from "../core/api";
import { IUser } from "../types";

export const User = types.model({
  id: types.identifierNumber,
  name: types.string,
  email: types.string,
  gender: types.union(types.literal("male"), types.literal("female")),
  status: types.union(types.literal("active"), types.literal("inactive")),
});

export const UserModel = types
  .model({
    users: types.optional(types.array(User), []),
    currentPage: types.optional(types.number, 1),
    status: types.union(
      types.literal("pending"),
      types.literal("done"),
      types.literal("error")
    ),
  })
  .actions((self) => ({
    getNewUsers: flow(function* () {
      self.status = "pending";

      try {
        const { data } = yield getUsersFromApi(self.currentPage);

        self.users = [...self.users, ...data.data] as typeof self.users;
        self.status = "done";
        self.currentPage = self.currentPage + 1;

        return data.data as IUser[];
      } catch (e) {
        self.status = "error";
      }

      return [];
    }),
  }));
