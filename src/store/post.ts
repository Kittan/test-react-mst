import { flow, types } from "mobx-state-tree";
import { getUserPostsFromApi } from "../core/api";

export const Post = types.model({
  id: types.identifierNumber,
  user_id: types.number,
  title: types.string,
  body: types.string,
});

export const PostModel = types
  .model({
    posts: types.optional(types.map(types.array(Post)), {}),
    status: types.union(
      types.literal("pending"),
      types.literal("done"),
      types.literal("error")
    ),
  })
  .actions((self) => ({
    getUserPosts: flow(function* (id: string) {
      self.status = "pending";

      try {
        const { data } = yield getUserPostsFromApi(id);

        self.posts.set(id, data.data)
        self.status = "done";
        return data.data;
      } catch (e) {
        self.status = "error";
      }
    }),
    getCountPostsFromUser: (user_id: string) => {
      const currentUser = self.posts.get(user_id)
      return currentUser?.length;
    },
  }));
