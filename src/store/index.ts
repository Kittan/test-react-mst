import { flow, Instance, types } from "mobx-state-tree";
import { createContext, useContext } from "react";
import { IUser } from "../types";

import { PostModel } from "./post";
import { UserModel } from "./user";

export type RootInstance = Instance<typeof RootModel>;

const RootModel = types
  .model({
    userModel: UserModel,
    postModel: PostModel,
    status: types.union(
      types.literal("pending"),
      types.literal("done"),
      types.literal("error")
    ),
  })
  .actions((self) => ({
    getUsersAndPosts: flow(function* () {
      self.status = "pending";

      try {
        const newUsers =
          (yield self.userModel.getNewUsers()) as unknown as IUser[];

        for (let user of newUsers) {
          const userPosts = yield self.postModel.getUserPosts(String(user.id));

          self.postModel.posts.set(String(user.id), userPosts || []);
        }
        self.status = "done";
      } catch (error) {
        console.log(error);
        self.status = "error";
      }
    }),
  }));

const initialState = RootModel.create({
  userModel: {
    users: [],
    currentPage: 1,
    status: "pending",
  },
  postModel: {
    posts: {},
    status: "pending",
  },
  status: "pending",
});

export const rootStore = initialState;

const RootStoreContext = createContext<null | RootInstance>(rootStore);

export const StoreProvider = RootStoreContext.Provider;

export function useMst() {
  const store = useContext(RootStoreContext);

  if (store === null) {
    throw new Error("Store cannot be null, please add a context provider");
  }

  return store;
}

export * from "./user";
export * from "./post";
