import { values } from "mobx";
import { observer } from "mobx-react-lite";
import { ReactElement, useEffect, useMemo } from "react";
import { Column, useBlockLayout, useTable } from "react-table";

import { Table } from "../components/table";
import { IUser } from "../types";
import { useMst } from "../store";
import { ToastContainer } from "react-toastify";

export const Users = observer((): ReactElement => {
  const rootModel = useMst();
  const { userModel } = rootModel;

  const users = values(userModel.users);
  const data = useMemo(() => users, [users]);
  const columns = useMemo<Array<Column<IUser>>>(
    () => [
      { Header: "ID", accessor: "id", width: 50 },
      {
        Header: "Name",
        accessor: "name",
        width: 300,
      },
      {
        Header: "Email",
        accessor: "email",
        width: 400,
      },
      {
        Header: "Gender",
        accessor: "gender",
        width: 100,
      },
      {
        Header: "Status",
        accessor: "status",
        width: 100,
      },
    ],
    []
  );

  const tableInstance = useTable({ columns, data }, useBlockLayout);

  useEffect(() => {
    rootModel.getUsersAndPosts();
  }, [rootModel]);

  useEffect(() => {
    const virtualList = document.querySelector(".virtual-list");

    const handleGetUsersAndPosts = (e: Event) => {
      if (!virtualList) return;

      const isMaxList =
        virtualList.scrollTop + virtualList.clientHeight ===
        virtualList.scrollHeight;

      if (isMaxList) rootModel.getUsersAndPosts();
    };

    virtualList?.addEventListener("scroll", handleGetUsersAndPosts);

    return () => {
      virtualList?.removeEventListener("scroll", handleGetUsersAndPosts);
    };
  }, [rootModel]);

  return (
    <>
      <Table instance={tableInstance} />
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
});
